# gitlab-ci-rust-template

A template to have an easy start with Rust projects using GitLab CI.

It requires using a runner with container support.

## Usage

In your `gitlab-ci.yml` add the following lines:

```yaml
include:
  - project: "okin/gitlab-ci-rust-template"
    file: "templates/gitlab-ci-rust.yml"
```

If you are not on GitLab.com you can also include it directly:

```yaml
include:
  - remote: 'https://gitlab.com/okin/gitlab-ci-rust-template/-/raw/main/templates/gitlab-ci-rust.yml'
```

### Nextest

For the usage of [nextest](https://nexte.st/) as test runner you will have to setup a _ci_ profile.

In your project create the file .config/nextest.toml with the following content:

```toml
[store]
dir = "target/nextest"

[profile.default]
retries = 0
status-level = "pass"
final-status-level = "flaky"
failure-output = "immediate"
success-output = "never"
fail-fast = true

[profile.ci]
status-level = "skip"
# Print out output for failing tests as soon as they fail, and also at the end
# of the run (for easy scrollability).
failure-output = "immediate-final"
# Do not cancel the test run on the first failure.
fail-fast = false

[profile.ci.junit]
path = "junit.xml"
report-name = "nextest-run"
store-success-output = false
store-failure-output = true
```

## Configuration

The template can be configured through environment variables.

| Variable | Default Value | Choices | Description |
|---|---|---|---|
| `CARGO_HOME` | `cargo/cache` | | Local cache (as described in [cargo reference: environment variables)](https://doc.rust-lang.org/cargo/reference/environment-variables.html) |
| `RUST_CONTAINER_IMAGE` | `rust:latest` | | The container image to use for the Rust jobs |
| `CI_USE_NEXTEST` | `true` | `true` or `false` | Controls if [nextest](https://nexte.st/) should be used. Requires configuration of nextest if set to `true` |
| `CI_USE_CARGO_AUDIT` | `true` | `true` or `false` | This controls the use of [cargo audit](https://github.com/RustSec/rustsec/tree/main/cargo-audit) |
| `CI_CARGO_AUDIT_CAN_FAIL` |  | `true` | (Optional) if set to `true` `cargo audit` checks are allowed to fail |
| `PAGES_DEPLOYMENT` | `true` | `true` or `false` | If set to `true` rustdoc will be deployed as GitLab Pages |